import json
import logging
import pymysql
import pymysql.cursors
import os
import boto3
import botocore.exceptions


def lambda_handler(event, context):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    host = os.environ.get('DB_HOST')
    connection = None

    # checks if connection was successful
    try:
        connection = connect(host)
    except Exception as e:
        msg = "Failed to connect to database."
        error = str(e)
        response = errorResponse(msg, error, 400)
        return response

    error = "SUCCESS: Connection to RDS MySQL instance succeeded."
    logger.info(error)

    try:
        # Create a new list that contains all the anomalies
        fullList = []
        problemList = []

        # Select survey instances that have blank question texts.
        with connection.cursor() as cursor:
            query = """select mcq.surveyInstanceId, si.orgId, mcq.questionId from SurveyMCQ AS mcq 
                    INNER JOIN SurveyInstance as si ON si.id = mcq.surveyInstanceId
                    where mcq.surveyInstanceId IN (SELECT DISTINCT si.id FROM SurveyInstance as si 
                                INNER JOIN SurveyMCQ AS mcq ON si.id = mcq.surveyInstanceId
                                INNER JOIN OrganizationalUnit as ou ON ou.id = si.orgId
                                WHERE si.surveyClosedFlag = 0 AND si.orgId NOT IN (93,99,63) 
                                AND ou.parentId NOT IN (93,99,63) ORDER BY si.id) 
                    and (mcq.questionText = '' or mcq.questionText = null);"""
            cursor.execute(query)
            blankQuestionList = cursor.fetchall()
            if len(blankQuestionList) != 0:
                fullList.append(blankQuestionList)
                problemList.append(0)

        # Select survey instances that have duplicate MAQ/MCQ questions.
        with connection.cursor() as cursor:
            query = """select mcq.surveyInstanceId,si.orgId, mcq.questionId from SurveyInstance as si 
                    INNER JOIN SurveyMCQ as mcq ON si.id = mcq.surveyInstanceId
                    where mcq.surveyInstanceId IN(SELECT DISTINCT si.id FROM SurveyInstance as si 
                                INNER JOIN SurveyMCQ AS mcq ON si.id = mcq.surveyInstanceId
                                 INNER JOIN OrganizationalUnit as ou ON ou.id = si.orgId
                                WHERE si.surveyClosedFlag = 0 AND si.orgId NOT IN (93,99,63) 
                                AND ou.parentId NOT IN (93,99,63) ORDER BY si.id) 
                    GROUP BY si.id, mcq.questionId, mcq.questionText
                    HAVING COUNT(mcq.questionId) > 1
                    ORDER BY si.id;"""
            cursor.execute(query)
            duplicateQuestionList = cursor.fetchall()
            if len(duplicateQuestionList) != 0:
                fullList.append(duplicateQuestionList)
                problemList.append(1)

        # Select survey instances that have blank MAQ/MCQ responses.
        with connection.cursor() as cursor:
            query = """select DISTINCT mcq.surveyInstanceId,si.orgId, mcq.questionId from SurveyMCQAnswerDetail as mcqad 
                    INNER JOIN SurveyMCQ as mcq ON mcqad.surveyMCQId = mcq.id
                    INNER JOIN SurveyInstance as si ON si.id = mcq.surveyInstanceId
                    where mcq.surveyInstanceId IN (SELECT DISTINCT si.id FROM SurveyInstance as si 
                            INNER JOIN SurveyMCQ AS mcq ON si.id = mcq.surveyInstanceId
                            INNER JOIN OrganizationalUnit as ou ON ou.id = si.orgId
                            WHERE si.surveyClosedFlag = 0 AND si.orgId NOT IN (93,99,63) 
                            AND ou.parentId NOT IN (93,99,63) 
                            ORDER BY si.id)
                    and (mcqad.description = '' or mcqad.description = null) 
                    ORDER BY mcq.surveyInstanceId;"""
            cursor.execute(query)
            blankResponseList = cursor.fetchall()
            if len(blankResponseList) != 0:
                fullList.append(blankResponseList)
                problemList.append(2)

        # Select survey instances that have duplicate MAQ/MCQ responses.
        with connection.cursor() as cursor:
            query = """select mcq.surveyInstanceId,si.orgId, mcq.questionId from SurveyMCQAnswerDetail as mcqad 
                    INNER JOIN SurveyMCQ as mcq ON mcqad.surveyMCQId = mcq.id
                    INNER JOIN SurveyInstance as si ON si.id = mcq.surveyInstanceId
                    where mcq.surveyInstanceId IN (SELECT DISTINCT si.id FROM SurveyInstance as si INNER JOIN 
                            SurveyMCQ AS mcq ON si.id = mcq.surveyInstanceId
                            INNER JOIN OrganizationalUnit as ou ON ou.id = si.orgId
                            WHERE si.surveyClosedFlag = 0 AND si.orgId NOT IN (93,99,63) 
                            AND ou.parentId NOT IN (93,99,63)
                            ORDER BY si.id)
                    and (mcqad.description != '' or mcqad.description != null)
                    GROUP BY mcq.surveyInstanceId, mcq.questionId, mcqad.description
                    HAVING Count(mcqad.description) > 1
                    ORDER BY mcq.surveyInstanceId;"""
            cursor.execute(query)
            duplicateResponseList = cursor.fetchall()
            if len(duplicateResponseList) != 0:
                fullList.append(duplicateResponseList)
                problemList.append(3)

        # Select survey instances that have orphaned questions with no responses recorded at all
        with connection.cursor() as cursor:
            query = """Select DISTINCT smcq.surveyInstanceId, si.orgId, smcq.questionId
                        From SurveyMCQ smcq 
                        LEFT JOIN SurveyMCQAnswerDetail smcqad ON smcqad.surveyMCQId = smcq.id 
                        INNER JOIN SurveyInstance si ON si.id = smcq.surveyInstanceId 
                        INNER JOIN OrganizationalUnit ou ON ou.id = si.orgId 
                        and ou.id NOT IN (99,93,63) and ou.parentId NOT IN (93,99,63) 
                        WHERE smcqad.id IS NULL;"""
            cursor.execute(query)
            orphanedList = cursor.fetchall()
            if len(orphanedList) != 0:
                fullList.append(orphanedList)
                problemList.append(4)

        msg = "Full List: " + str(fullList)
        logger.info(msg)

        if len(fullList) != 0:
            SENDER = "MCQ Notifications <support@thelearningbar.com>"

            # Replace recipient@example.com with a "To" address.
            RECIPIENT = "mcqnotifications@thelearningbar.com"

            # Specify a configuration set. If you do not want to use a configuration
            # set, comment the following variable, and the 
            # ConfigurationSetName=CONFIGURATION_SET argument below.
            CONFIGURATION_SET = "ConfigSet"

            AWS_REGION = os.environ.get('AWS_REGION')

            # The subject line for the email.
            SUBJECT = "CA MCQ Notification"
            if AWS_REGION == "ap-southeast-2":
                SUBJECT = "AU MCQ Notification"

            # HTML style and defining the table headers.
            a = """<html>
            <head>
            <style>
                table{
                    border: 1px solid #ddd;
                    width: 50%
                }
                th{
                    background-color: green;
                    color: white;
                    text-align: left;
                    padding: 12px;
                    border: 1px solid #ddd;
                }
                td {
                    background-color: lightgray;
                    padding: 8px;
                    text-align: left;
                    border: 1px solid #ddd;
                }
            </style>
            </head>\n\t<body>\n\t<table>\n\t<tr>\n\t\t<th>surveyInstanceId</th>\n\t\t<th>orgId</th>\n\t\t<th>questionId</th>\n\t\t<th>problemType</th>\n\t</tr>"""
            b = ''

            # creates the table rows for the email
            counter = 0
            for lst in fullList:
                for obj in lst:
                    problemType = None
                    if problemList[counter] == 0:
                        problemType = "Blank Question"
                    elif problemList[counter] == 1:
                        problemType = "Duplicate Question"
                    elif problemList[counter] == 2:
                        problemType = "Blank Response"
                    elif problemList[counter] == 3:
                        problemType = "Duplicate Response"
                    elif problemList[counter] == 4:
                        problemType = "No Responses Recorded"

                    si = str(obj['surveyInstanceId'])
                    org = str(obj['orgId'])
                    q = str(obj['questionId'])

                    b += """\n\t<tr>\n""" + """\t\t<td>""" + si + """</td>\n"""
                    b += """\t\t<td>""" + org + """</td>\n"""
                    b += """\t\t<td>""" + q + """</td>\n"""
                    b += """\t\t<td>""" + problemType + """</td>\n\t</tr>"""

                counter += 1

            c = """\n\t</table>\n\t</body>\n</html>"""

            # Body of the email
            BODY_HTML = a + b + c

        else:
            body = {
                "Message": "No duplicate MCQ/MAQs were found."
            }
            bodyToJson = json.dumps(body, indent=4, default=str)
            response = createResponse(bodyToJson, 200)

            return response

            # The character encoding for the email.
        CHARSET = "UTF-8"

        # Create a new SES resource and specify a region.
        client = boto3.client('ses', region_name=AWS_REGION)

        # Try to send the email.
        try:
            # Provide the contents of the email.
            response = client.send_email(
                Destination={
                    'ToAddresses': [
                        RECIPIENT,
                    ],
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': CHARSET,
                            'Data': BODY_HTML,
                        },
                    },
                    'Subject': {
                        'Charset': CHARSET,
                        'Data': SUBJECT,
                    },
                },
                Source=SENDER,
                # ConfigurationSetName=CONFIGURATION_SET,
            )
        # Display an error if something goes wrong.

        except botocore.exceptions.ClientError as e:
            msg = str(e.response['Error']['Message'])
            logger.info(msg)
        else:
            msg = "Email sent! Message ID: " + str(response['MessageId'])
            logger.info(msg)

    except Exception as e:
        msg = "error"
        error = str(e)
        response = errorResponse(msg, error, 400)
        return response

    finally:
        connection.close()


# Used to connect to the database
def connect(host):
    connection = pymysql.connect(host=host, db=os.environ.get('DB_NAME'), user=os.environ.get('DB_USERNAME'),
                                 password=os.environ.get('DB_PASSWORD'), cursorclass=pymysql.cursors.DictCursor)
    return connection


def errorResponse(msg, error, statusCode):
    obj = {"message": msg, "error": error}
    objToJson = json.dumps(obj, indent=4, default=str)

    response = createResponse(objToJson, statusCode)
    return response


# creates the response JSON body
def createResponse(body, status):
    response = {
        "statusCode": status,
        "headers": {"Access-Control-Allow-Origin": os.environ.get('ORIGIN')},
        "body": body
    }

    return response
